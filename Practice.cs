﻿using System;

namespace Practica_cs
{

    partial class Practice
    {
        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            System.Console.WriteLine("Start the stopwatch");
            var start = Console.ReadLine();
            stopwatch.Start();
            
            System.Console.WriteLine("Stop the stopwatch");
            var stop = Console.ReadLine();
            stopwatch.Stop();

        }
    }
}

