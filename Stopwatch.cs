using System;

namespace Practica_cs
{
    public class Stopwatch
    {
        private DateTime startTime;



        public void Start()
        {
            startTime = DateTime.Now;
        }

        public void Stop()
        {
            DateTime stopTime = DateTime.Now;
            
            TimeSpan timeSpan = stopTime.Subtract(startTime);
            TimeSpan duration;
            duration =+timeSpan;

            System.Console.WriteLine(duration.ToString(@"hh\:mm\:ss\.fff"));
            
        }
    }

}
